import React from 'react';

const NavLink = () => {
    return (
        <div>
            <NavLink>All</NavLink>
            <NavLink>Stars-wars</NavLink>
            <NavLink>Comedy</NavLink>
            <NavLink>Cinema</NavLink>
            <NavLink>Motivational</NavLink>
        </div>
    );
};

export default NavLink;