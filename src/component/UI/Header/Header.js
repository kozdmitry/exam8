import React from 'react';
import "./Header.css";
import {NavLink} from "react-router-dom";

const Header = () => {
    return (
        <div className="Container">
            <div className="Header">
                <h2>Quotes Central</h2>
                <div className="nav">
                    <NavLink className="link" to={'/'}>Quotes</NavLink>
                    <p> / </p>
                    <NavLink className="link" to={'/add'}>Submit new quote</NavLink>
                </div>
            </div>
            <div className="navBlock">
                <NavLink className="linkMenu" to={'/'}>All</NavLink>
                <NavLink className="linkMenu" to={'/quotes.json?orderBy="category"&equalTo="starWars"'}>Star-wars</NavLink>
                <NavLink className="linkMenu" to={'/comedy'}>Comedy</NavLink>
                <NavLink className="linkMenu" to={'/cinema'}>Cinema</NavLink>
                <NavLink className="linkMenu" to={'/motivational'}>Motivational</NavLink>
            </div>
        </div>

    );
};

export default Header;