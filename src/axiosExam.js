import axios from 'axios';

const AxiosExam = axios.create({
    baseURL: 'https://js-9-exam8-kozyrev-default-rtdb.firebaseio.com/',
});

export default AxiosExam;