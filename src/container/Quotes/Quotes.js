import React, {useEffect, useState} from 'react';
import axiosExam from "../../axiosExam";
import {NavLink} from "react-router-dom";
import "./Quotes.css";


const Quotes = () => {

    const [quotes, setQuotes] = useState([]);

        useEffect(() => {
            axiosExam('/posts/.json').then(response => {
                console.log(response);
                setQuotes(response.data)
                console.log(setQuotes);
            }, e => {
                console.log(e);
            });
        });

    return (
        <div className="quotes">
            {Object.keys(quotes).map(key => (
                <div className="postList" key={key}>
                    <p>{quotes[key].text}</p>
                    <b>****{quotes[key].author}</b>
                    <NavLink to={`/posts/${key}/edit`} className="btn">Edit</NavLink>
                    <NavLink to={`posts/${key}/delete`} className="btn">X</NavLink>
                </div>
            ))}
        </div>
    );
};

export default Quotes;