import React, {useState, useEffect} from 'react';
import axiosExam from "../../axiosExam";

const Edit = props => {
    const id = props.match.params.id;

    const [editQuotes, setEditQuotes] = useState({
        author: '',
        text: '',
        category: ''
    });

    useEffect(() => {
        axiosExam.get(`/posts/${id}.json`).then(response => {
            setEditQuotes(response.data)
        });
    }, [id]);

    const editDataChanged = event => {
        const name = event.target.name;
        const value = event.target.value;

        setEditQuotes(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const postEditHandler = () => {
        const editPosts = {
            author: editQuotes.author,
            category: editQuotes.category,
            text: editQuotes.text
        }
        axiosExam.put(`/posts/${id}.json`, editPosts).then(() => {
            props.history.push('/');
        })
    };

    return (
        <div className={Edit}>
            <h2>Submit edit quote</h2>
            <form className="form" onSubmit={postEditHandler}>
                <label for="category">Category</label>
                <select name="category" onChange={editDataChanged}>
                    <option value="starWars">StarWars</option>
                    <option value="comedy">Comedy</option>
                    <option value="cinema">Cinema</option>
                    <option value="motivational">Motivational</option>
                </select>
                <input type="text"
                       placeholder="Author"
                       name="author"
                       value={editQuotes.author}
                       onChange={editDataChanged}
                />
                <textarea
                    className="Input"
                    type="text" name="text"
                    placeholder="enter your text"
                    value={editQuotes.text}
                    onChange={editDataChanged}
                />
                <button className="btnEdit">Edit post</button>
            </form>

        </div>
    );
};

export default Edit;