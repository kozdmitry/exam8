import React, {useState} from 'react';
import "./Add.css";
import axiosExam from "../../axiosExam";


const Add = props => {
    // const id = props.match.params.id;

    const [post, setPost] = useState({});

    const postDataChanged = event => {
        const name = event.target.name;
        const value = event.target.value;

        setPost(prevState => ({
            ...prevState,
            [name]: value
        }));
    };
    const postHandler = async (e) => {
        e.preventDefault();

        const posts = {
            author: post.author,
            category: post.category,
            text: post.text
        }

        try {
            await axiosExam.post('/posts.json', posts);
        } finally {
            props.history.push('/');
        }
    };

    return (
        <div className={Add}>
            <h2>Submit new quote</h2>
            <form className="form" onSubmit={postHandler}>
                <label for="category">Category</label>
                <select className="input" name="category" onChange={postDataChanged}>
                    <option value="starWars">StarWars</option>
                    <option value="comedy">Comedy</option>
                    <option value="cinema">Cinema</option>
                    <option value="motivational">Motivational</option>
                </select>
                <input
                    className="input"
                    type="text"
                    placeholder="Author"
                    name="author"
                    value={post.author}
                    onChange={postDataChanged}
                />
                <textarea
                    cols="20"
                    rows="5"
                    className="Input"
                    type="text" name="text"
                    placeholder="enter your text"
                    value={post.text}
                    onChange={postDataChanged}
                />
                <button className="btnSend">Send</button>
            </form>

        </div>
    );
};

export default Add;