import React from 'react';
import './App.css';
import Header from "./component/UI/Header/Header";
import {BrowserRouter as Router, Switch, Route, BrowserRouter} from "react-router-dom";
import Quotes from "./container/Quotes/Quotes";
import Add from "./container/Add/Add";
import NavLink from "./component/UI/NavLink/NavLink";

const App = () => {
  return (
    <div className="App">

      <BrowserRouter>
          <Header />

          <Switch>
              <Route path={'/'} exact component={Quotes}/>
              <Route path={'/add'} exact component={Add}/>
          </Switch>

      </BrowserRouter>

    </div>
  );
}

export default App;
